class SarsCov2{
    #radio;
    #xCentro;
    #yCentro;
    #colorContorno;
    #colorRelleno;
    #vx;
    #vy;

    constructor(radioInicial, xInicial, yInicial, colorContorno, colorRelleno)
    {
        this.#radio = radioInicial || 30;
        this.#xCentro = xInicial || 100;
        this.#yCentro = yInicial || 50;
        this.#colorContorno = colorContorno || "blue";
        this.#colorRelleno = colorRelleno || "green";
        this.#vx = 2;
        this.#vy = -3;
    }
    dibujar (ctx)
    {
        ctx.beginPath();
        ctx.arc(this.#xCentro,this.#yCentro,this.#radio,0, 2.0 * Math.PI);
        ctx.fillStyle = this.#colorRelleno;
        ctx.fill();
        ctx.strokeStyle = this.#colorContorno;
        ctx.stroke();
        ctx.closePath();
    }
}