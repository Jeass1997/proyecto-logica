class Personaje{
    #radio;
    #xCentro;
    #yCentro;
    #colorContorno;
    #colorRelleno;
    

    constructor(radioInicial, xInicial, yInicial, colorContorno, colorRelleno)
    {
        this.#radio = radioInicial || 30;
        this.#xCentro = xInicial || 100;
        this.#yCentro = yInicial || 50;
        this.#colorContorno = colorContorno || "blue";
        this.#colorRelleno = colorRelleno || "green";
    }
    dibujar (ctx)
    {
        ctx.beginPath();
        ctx.arc(this.#xCentro,this.#yCentro,this.#radio,0, 2.0 * Math.PI);
        ctx.fillStyle = this.#colorRelleno;
        ctx.fill();
        ctx.strokeStyle = this.#colorContorno;
        ctx.stroke();
        ctx.closePath();
    }
}